@extends('layouts.app')

@section('content')
<div class="container">
    <h2 class="text-primary">{{ $user->username }}</h2>
    <hr>
   
    <br><br>
    @foreach($user->posts as $post)
    <div>
        <div class="lead">{{ $post->body }}</div>
        <div class="text-muted">{{ $post->humanCreatedAt }}</div>
        <hr>
    </div>
    @endforeach

</div>
@endsection
